package com.example.demo.service.application;

import com.example.demo.data.entity.ApplicationStatusEntity;
import com.example.demo.data.dto.ApplicationUpdate;
import com.example.demo.data.dto.StatusUpdate;
import org.springframework.data.domain.Page;

public interface ApplicationService {
    Page<ApplicationStatusEntity> getAllApplications(Integer pageNumber, Integer pageSize, String sortBy);

    boolean updateApplicationStatus(StatusUpdate statusUpdate);

    boolean updateApplication(ApplicationUpdate applicationUpdate);
}
