package com.example.demo.service.application;

import com.example.demo.data.dto.ApplicationUpdate;
import com.example.demo.data.entity.ApplicationEntity;
import com.example.demo.data.entity.ApplicationStatusEntity;
import com.example.demo.data.dto.StatusUpdate;
import com.example.demo.repository.ApplicationRepository;
import com.example.demo.repository.ApplicationStatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ApplicationServiceImpl implements ApplicationService {

    private final ApplicationStatusRepository applicationStatusRepository;

    private final ApplicationRepository applicationsRepository;

    @Override
    public Page<ApplicationStatusEntity> getAllApplications(Integer pageNumber, Integer pageSize, String sortBy) {
        Pageable pageable;
        if (sortBy == null) {
            pageable = PageRequest.of(pageNumber, pageSize);
        } else {
            Sort sort = Sort.by(Sort.Order.asc(sortBy));
            pageable = PageRequest.of(pageNumber, pageSize, sort);
        }
        return applicationStatusRepository.findAll(pageable);
    }

    @Override
    public boolean updateApplicationStatus(StatusUpdate statusUpdate) {
        Optional<ApplicationEntity > applicationEntityOptional = applicationsRepository.findById(statusUpdate.getApplication_id());
        ApplicationEntity applicationEntity = applicationEntityOptional.orElse(null);
        ApplicationStatusEntity applicationStatusEntity = new ApplicationStatusEntity();
        if (applicationEntity !=null) {
            applicationStatusEntity.setStatusEnum(statusUpdate.getNewStatus());
            applicationStatusEntity.setStatusTime(Instant.now());
            applicationStatusRepository.save(applicationStatusEntity);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean updateApplication(ApplicationUpdate applicationUpdate) {
        Optional<ApplicationEntity > applicationEntityOptional = applicationsRepository.findById(applicationUpdate.getApplication_id());
        ApplicationEntity applicationEntity = applicationEntityOptional.orElse(null);
        if (applicationEntity !=null) {
            applicationEntity.setText(applicationUpdate.getText());
            applicationsRepository.save(applicationEntity);
            return true;
        } else {
            return false;
        }
    }

}
