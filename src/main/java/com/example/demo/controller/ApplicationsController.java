package com.example.demo.controller;

import com.example.demo.data.dto.ApplicationDto;
import com.example.demo.data.dto.ApplicationUpdate;
import com.example.demo.data.dto.StatusUpdate;
import com.example.demo.data.entity.ApplicationEntity;
import com.example.demo.data.entity.ApplicationStatusEntity;
import com.example.demo.repository.ApplicationRepository;
import com.example.demo.service.application.ApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("applications")
@RequiredArgsConstructor
public class ApplicationsController {

    private final ApplicationService applicationService;

    private final ApplicationRepository applicationRepository;

    @GetMapping("getAllPageable")
    public Page<ApplicationStatusEntity> getApplications(@RequestParam Integer pageNumber, @RequestParam(required = false) String sortBy) {
        return applicationService.getAllApplications(pageNumber, 10, sortBy);
    }

    @GetMapping("get")
    public List<ApplicationEntity> getApplications() {
        return applicationRepository.findAll();
    }

    @PostMapping("save")
    public ApplicationEntity saveApplication(@RequestBody ApplicationDto applicationDto) {
        return applicationRepository.save(applicationDto.getApplication());
    }

    @PutMapping("updateApplicationText")
    public HttpStatus updateApplicationText(ApplicationUpdate applicationUpdate) {
        if (applicationService.updateApplication(applicationUpdate)) {
            return HttpStatus.NO_CONTENT;
        } else {
            return HttpStatus.BAD_REQUEST;
        }
    }

    @PutMapping("updateApplicationStatus")
    public HttpStatus update(StatusUpdate statusUpdate) {
        if (applicationService.updateApplicationStatus(statusUpdate)) {
            return HttpStatus.NO_CONTENT;
        } else {
            return HttpStatus.BAD_REQUEST;
        }
    }

}
