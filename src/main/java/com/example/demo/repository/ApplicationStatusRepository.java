package com.example.demo.repository;

import com.example.demo.data.entity.ApplicationStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationStatusRepository extends JpaRepository<ApplicationStatusEntity, Long> {
}
