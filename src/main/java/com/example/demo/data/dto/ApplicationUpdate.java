package com.example.demo.data.dto;

import com.example.demo.data.entity.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ApplicationUpdate {

    Long application_id;

    String text;

    Instant statusChangeTime;

}
