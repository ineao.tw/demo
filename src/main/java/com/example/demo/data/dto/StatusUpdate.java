package com.example.demo.data.dto;

import com.example.demo.data.entity.StatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StatusUpdate {

    Long application_id;

    StatusEnum newStatus;

    Instant statusChangeTime;

}
