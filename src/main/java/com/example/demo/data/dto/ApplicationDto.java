package com.example.demo.data.dto;

import com.example.demo.data.entity.ApplicationEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ApplicationDto {

    private ApplicationEntity application;
}