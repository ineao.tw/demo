package com.example.demo.data.entity;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "application_status")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApplicationStatusEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "status_time")
    private Instant statusTime;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private StatusEnum statusEnum;

    @Column(name = "reason")
    private String reason;

}