package com.example.demo.data.entity;

public enum StatusEnum {
    CREATED, VERIFIED, ACCEPTED
}
