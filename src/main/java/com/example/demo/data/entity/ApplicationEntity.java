package com.example.demo.data.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "application")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApplicationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "application_id")
    private Long applicationId;

    @Column(name = "title")
    private String title;

    @Column(name = "text")
    private String text;

    @OneToMany(targetEntity= ApplicationStatusEntity.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "application_status", referencedColumnName = "application_id")
    private Set<ApplicationStatusEntity> applicationStatuses;
}
